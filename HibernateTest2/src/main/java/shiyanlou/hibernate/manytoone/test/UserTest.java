package shiyanlou.hibernate.manytoone.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import shiyanlou.hibernate.manytoone.entity.Group;
import shiyanlou.hibernate.manytoone.entity.User;

public class UserTest {

    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        // 开启事务
        session.beginTransaction();

        Group group = new Group();
        group.setGroupname("LOL_Group");

        // 新建多个用户
        User user1 = new User();
        user1.setUsername("Levis");
        user1.setPassword("111");
        user1.setGroup(group);
        
        User user2 = new User();
        user2.setUsername("Lee");
        user2.setPassword("2");
        user2.setGroup(group);

        session.save(group);
        session.save(user1);
        session.save(user2);

        // 提交事务并关闭 session
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}