#!/usr/bin/env python3
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

def callback(ch, method, properties, body):
    print("[x] Received %r" % body)

# 告诉 RabbitMQ 这个回调函数将会从名为 "hello" 的队列中接收消息
channel.basic_consume(queue='hello', auto_ack=True, on_message_callback=callback)

print('[x] Waiting for message. To exit press CTRL+C')
# 等待消息数据并且在需要的时候运行回调函数的无限循环
channel.start_consuming()