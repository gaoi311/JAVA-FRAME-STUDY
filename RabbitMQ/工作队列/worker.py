import pika
import time

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello', durable=True)

def callback(ch, method, properties, body):
    print("[x] Received %r" % body)
    time.sleep(body.count(b'.'))
    print("[x] Done")

# 直到处理完上一个才继续接收
channel.basic_qos(prefetch_count=1)

# 消息从队列异步推送给消费者，消费者的 ack 也是异步发送给队列，从队列的视角去看
# 总是会有一批消息已推送但尚未获得 ack 确认，Qos 的 prefetchCount 参数就是用来限制这批未确认消息数量的。
# 设为1时，队列只有在收到消费者发回的上一条消息 ack 确认后，才会向该消费者发送下一条消息。
# prefetchCount 的默认值为0，即没有限制，队列会将所有消息尽快发给消费者
channel.basic_consume(queue='hello', auto_ack=True, on_message_callback=callback)

print("[x] Waiting for message.To exit press CTRL+C")

channel.start_consuming()