import pika
import sys

connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))
channel = connection.channel()

message = ' '.join(sys.argv[1:]) or "hello World"
channel.basic_publish(exchange='', routing_key='hello', body=message)
print("[x] Sent %r" % message)