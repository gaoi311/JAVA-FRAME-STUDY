package com.shiyanlou.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import com.shiyanlou.spring.jdbc.entity.Student;
import java.util.List;

public class App {
    private static ApplicationContext context;

    public static void main(String[] args) {
        context = new ClassPathXmlApplicationContext("SpringBeans.xml");

        JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
        // 增
        // String sql = "insert into student values(?,?,?)";
        // int count = jdbcTemplate.update(sql,new Object[]{4,"shiyanlou3",18});
        // System.out.println(count);
        // 删
        // String sql = "delete from student where id=?";
        // int count = jdbcTemplate.update(sql, 2);
        // System.out.println(count);
        // 改
        // String sql = "update student set name=?, age=? where id=?";
        // int count = jdbcTemplate.update(sql, new Object[]{"shiyanlou1", 20, 1});
        // System.out.println(count);
        // 查
        String sql = "select * from student";
        RowMapper<Student> rowMapper = new BeanPropertyRowMapper<Student>(Student.class);
        List<Student> students = jdbcTemplate.query(sql, rowMapper);
        for( Student student: students){
            System.out.println(student);
        }
        
    }
}