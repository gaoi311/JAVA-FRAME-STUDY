package com.shiyanlou.spring.aop.aspectj;

public class CustomerBo implements ICustomerBo{
    public void addCustomer(){
        System.out.println("addCustomer() is running...");
    }

    public vlid deleteCustomer(){
        System.out.println("deleteCustomer() is running...");
    }

    public String AddCustomerReturnValue(){
        System.out.println("AddCustomerThrowException() is running...");
    }

    public void addCustomerThrowException() throws Exception {
        System.out.println("addCustomerThrowException() is running...");
    }

    public void addCustomerAround(String name){
        System.out.println("addCustomerAroung() is running, args :" + name);
    }

}