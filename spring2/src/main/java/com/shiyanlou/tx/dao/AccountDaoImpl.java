package com.shiyanlou.tx.dao;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.shiyanlou.tx.dao.AccountDao;
public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao{
    public void out(String outer, int money){
        this.getJdbcTemplate().update("update account set money = money - ? where useranme=?", money, outer);
    }

    public void in(String inner, int money){
        this.getJdbcTemplate().update("update account set money = money + ? where useranme=?", money, inner);
    }
}