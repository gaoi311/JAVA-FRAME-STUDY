package com.shiyanlou.springboot;

import com.shiyanlou.springboot.first.UserOne;
import com.shiyanlou.springboot.first.UserOneService;
import com.shiyanlou.springboot.second.UserSecond;
import com.shiyanlou.springboot.second.UserSecondService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
    ApplicationRunner 接口可以让在SpringBoot启动后马上执行想要执行的方法
 */

    @Component
    public class DataSourceTest implements ApplicationRunner {
        @Autowired
        public UserOneService userOneService;

        @Autowired
        public UserSecondService userSecondService;

        public void run(ApplicationArguments args){
        // 新建一个实体类
        UserOne userOne = new UserOne();
        userOne.setUsername("shiyanlou1");
        userOne.setPassword("springboot1");
        // 调用 Service
        userOneService.save(userOne);
        UserSecond userSecond = new UserSecond();
        userSecond.setPassword("shiyanlou2");
        userSecond.setUsername("springBoot2");
        //调用 Service
        userSecondService.save(userSecond);
    }
}