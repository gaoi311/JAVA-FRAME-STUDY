package com.shiyanlou.springboot.first;

import org.springframework.data.repository.CrudRepository;

public interface UserOneRepository extends CrudRepository<UserOne, Integer>{}