package com.shiyanlou.springboot.second;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserSecondService{
    @Autowired
    private UserSecondRepository userSecondRepository;

    @Transactional(rollback = Exception.class)
    public UserSecond save(UserSecond userSecond){
        // 保存实体类
        return userSecondRepository.save(userSecond);
    }
}