package com.shiyanlou.springboot.second;

import org.springframework.data.repository.CrudRepository;

public interface UserSecondRepository extends CrudRepository<UserSecond, Integer>{}