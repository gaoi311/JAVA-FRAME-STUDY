package shiyanlou.test.hibernate.test;

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.cfg.Configuration;

import shiyanlou.test.hibernate.entity.User;

public class Test{
    public static void main(String[] args) {

        // 获取 Hibernate 配置信息
        Configuration configuration = new Configuration().configure();
        // 根据 configuration 建立 sessionFactory
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // 开启 session（相当于开启 JDBC 的 connection）
        Session session = sessionFactory.openSession();

        // 创建并开启事务对象
        session.beginTransaction();

        // 新建对象，并赋值
        User user = new User();
        user.setId(1);
        user.setUsername("admin");
        user.setPassword("admin");

        // 保存对象
        session.save(user);

        // 提交事务
        session.getTransaction().commit();

        System.out.println("标量查询");
        //标量查询
        session.beginTransaction();

        String sql1 = "SELECT user_username, user_password FROM user_info";
        SQLQuery query1 = session.createSQLQuery(sql1);
        query1.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List data1 = query1.list();

        for(Object object : data1)
        {
            Map row = (Map)object;
            System.out.print("username: " + row.get("user_username"));
            System.out.println(", password: " + row.get("user_password"));
        }

        session.getTransaction().commit();

        System.out.println("实体查询");
        //实体查询
        session.beginTransaction();

        String sql2 = "SELECT * FROM user_info";
        SQLQuery query2 = session.createSQLQuery(sql2);
        query2.addEntity(User.class);
        List users = query2.list();

        for (Iterator iterator = users.iterator(); iterator.hasNext();){
            User user2 = (User) iterator.next();
            System.out.print("id: " + user2.getId());
            System.out.print("  username: " + user2.getUsername());
            System.out.println("  password: " + user2.getPassword());
         }

        session.getTransaction().commit();

        System.out.println("指定 SQL 查询");
        //指定 SQL 查询
        session.beginTransaction();

        String sql3 = "SELECT * FROM user_info WHERE user_id = :user_id";
        SQLQuery query3 = session.createSQLQuery(sql3);
        query3.addEntity(User.class);
        query3.setParameter("user_id", 1);
        List results = query3.list();

        for (Iterator iterator = users.iterator(); iterator.hasNext();){
            User user3 = (User) iterator.next();
            System.out.print("id: " + user3.getId());
            System.out.print("  username: " + user3.getUsername());
            System.out.println("  password: " + user3.getPassword());
         }

        session.getTransaction().commit();

        // 关闭 session 和 sessionFactory
        session.close();
        sessionFactory.close();
    }
}