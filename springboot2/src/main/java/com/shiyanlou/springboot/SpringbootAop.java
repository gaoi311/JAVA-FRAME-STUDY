package com.shiyanlou.springboot;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionInterceptor;

// 声明为切面类
@Aspect
@Component
public class SpringbootAop{


  // 设置切点
  @Pointcut(value = "execution(* com.shiyanlou.springboot..*.run(..))")
  public void aop(){ }

  @Before("aop()")
  public void before(){
    System.out.println("before：执行方法前");
  }


  @After("aop()")
  public void after(){
    System.out.println("after：执行方法后");
  }

  @AfterReturning("aop()")
  public void afterReturning(){
    System.out.println("afterReturning：方法返回后");
  }

  @AfterThrowing("aop()")
  public void afterThrowing(){
    System.out.println("afterThrowing：异常抛出后");
  }

  @Around("aop()")
  public void around(ProceedingJoinPoint joinPoint) throws Throwable{

    System.out.println("around：环绕通知前");
    joinPoint.proceed();
    System.out.println("run 方法执行");
    System.out.println("around：环绕通知后");

  }
}