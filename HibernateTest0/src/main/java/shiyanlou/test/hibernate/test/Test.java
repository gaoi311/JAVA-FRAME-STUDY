package shiyanlou.test.hibernate.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import shiyanlou.test.hibernate.entity.User;

import java.util.List;

public class Test {

    public static void main(String[] args) {

        /* 增
        // 获取 Hibernate 配置信息
        Configuration configuration = new Configuration().configure();
        @SuppressWarnings("deprecation")
        // 根据 configuration 建立 sessionFactory
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        // 开启 session（相当于开启 JDBC 的 connection）
        Session session = sessionFactory.openSession();

        // 创建并开启事务对象
        session.beginTransaction();

        // 新建对象，并赋值
        User user = new User();
        user.setId(1);
        user.setUsername("admin");
        user.setPassword("admin");

        // 保存对象
        session.save(user);

        // 提交事务
        session.getTransaction().commit();

        // 关闭 session 和 sessionFactory
        session.close();
        sessionFactory.close();
        */

        /* 查
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        // 利用 StringBuilder 来连接查询语句
        StringBuilder hq = new StringBuilder();

        // 从 User 里面查找（注意 from 后有空格）
        // 相当于 "select * from user_info;"
        hq.append("from ").append( User.class.getName() );

        // 利用 session 建立 query
        Query query = session.createQuery( hq.toString() );

        // 序列化 query 的结果为一个 list 集合
        List<User> users = query.list();

        // 打印每一个 User 信息（这里只打印了名字，你也可以打印其他信息）
        for (User user : users) {
            System.out.println( user.getUsername() );

        }

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        */

        /* 改
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        StringBuilder hq = new StringBuilder();

        // 对比查找的操作来看，因为我们需要修改指定 name 的用户密码，后面需要再添加查询条件
        // 注意 from、where 的空格，":name" 表示一个参数
        hq.append("from ").append(User.class.getName()).append(" where user_username=:name");

        Query query = session.createQuery(hq.toString());

        // 这里就设定参数 name 的值为"user1"
        query.setParameter("name","user1" );

        List<User> users = query.list();

        for (User user : users) {

            // 修改 user1 的密码
            user.setPassword("123-user");

            // 注意这里是 update
            session.update(user);
        }

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        */

        /* 删
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        StringBuilder hq = new StringBuilder();

        // 对比查找时候的操作来看，因为我们需要修改指定 name 的用户密码，后面需要再添加查询条件
        // 注意 from、where 的空格，":name" 表示一个参数
        hq.append("from ").append(User.class.getName()).append(" where user_username=:name");

        Query query = session.createQuery(hq.toString());

        // 这里就设定参数 name 的值为"user1"
        query.setParameter("name", "user1");

        List<User> users = query.list();

        for (User user : users) {
            // 注意这里是 delete
            session.delete(user);
        }

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        */
    }
}