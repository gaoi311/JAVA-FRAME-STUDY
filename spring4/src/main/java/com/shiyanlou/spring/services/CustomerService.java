package com.shiyanlou.spring.services;

import com.shiyanlou.spring.dao.CustomerDAO;

@Component  // 自动扫描
public class CustomerService{

    @Autowired  // 自动装配
    CustomerDAO customerDAO;

    public void setCustomerDAO(CustomerDAO customerDAO){
        this.customerDAO = customerDAO;
    }

    @Override
    public String toString(){
        return "CustomerService [customerDAO = " + customerDAO + "]";
    }
}