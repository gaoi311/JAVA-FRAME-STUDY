package shiyanlou.hibernate.hql.test;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import shiyanlou.hibernate.hql.entity.User;

import java.util.List;

public class UserTest {

    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

        Session session = sessionFactory.openSession();

        session.beginTransaction();

        // 1. 普通查询
        Query q = session.createQuery(" from User as u");

        // 2. 条件查询
//        Query q = session.createQuery(" from User as u where u.username = ?0");
//        q.setParameter(0, "Jack");

        // 3. 原生 SQL 查询
//        NativeQuery q = session.createSQLQuery("select * from user_info").addEntity(User.class);

        // 4.criteria 查询
//        Criteria q = session.createCriteria(User.class);
//        Criterion cc = Restrictions.between("id", 1, 3);
//        Criterion cc1 = Restrictions.idEq(2);
//        q.add(cc);
//        q.add(cc1);

        @SuppressWarnings("unchecked")
        List<User> list = q.list();
        for (User e : list) {
            System.out.println(e.getUsername() + ", password: " + e.getPassword());
            //System.out.println( e.getPassword() );
        }

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();

    }
}