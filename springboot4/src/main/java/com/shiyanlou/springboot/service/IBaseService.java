package com.shiyanlou.springboot.service;

import java.util.List;

public interface IBaseService<T>{
    Integer save(T t);

    void delete(Integer id);

    T findById(Integer id);

    void update(T t);

    List<T> list();
}