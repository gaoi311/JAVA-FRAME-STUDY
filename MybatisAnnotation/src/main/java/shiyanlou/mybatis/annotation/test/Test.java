package shiyanlou.mybatis.annotation.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.ibatis.ibatis.session.SqlSessionFactoryBuilder;

import shiyanlou.mybatis.annotation.mapper.UserMapper;
import shiyanlou.mybatis.annotation.model.User;

public class Test{
    private static SqlSessionFactory sqlSessionFactory;

    public static void main(String[] args){
        String resource = "mybatis.cfg.xml";

        InputStream inputStream = null;
        try{
            inputStream = Resources.getResourceAsStream(resource);
        }catch(IOException e){
            e.printStackTrace();
        }

        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        inertUser();
    }

    private static void insertUser(){
        SqlSession session = sqlSessionFactory.openSession();

        UserMapper mapper = session.getMapper(UserMapper.class);
        User user = new User();
        User.setName("Anne");
        User.setSex("female");
        User.setAge(23);
        try{
            mapper.insertUser(user);
            session.commit();
        }catch(Exception e){
            e.printStackTrace();
            session.rollback();
        }
        session.close();
    }
}