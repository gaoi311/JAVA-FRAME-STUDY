<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags"  prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>File Uploaded</title>
</head>

<body>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
upload succeed!<br/>
uploader:<s:property value=" + uploader"/><br/>
file name:<s:property value=" + uploadFileName"/><br/>
file type:<s:property value=" + uploadContentType"/><br/>
file address:<p> <%basePath%><s:property value="'uploaderFiles/' + uploadFileName" /></p><br/>
</body>
</html>