package com.shiyanlou.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShiyanlouController{
    @RequestMapping("shiyanlou")
    public String shiyanlou(){
        return "shiyanlou";
    }
}