package com.shiyanlou.springboot.config;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;


//@Configuration
public class ServletConfig implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>{
  @Override
  public void customize(ConfigurableServletWebServerFactory factory){
    // 设置端口
    factory.setPort(8080);
  }
}