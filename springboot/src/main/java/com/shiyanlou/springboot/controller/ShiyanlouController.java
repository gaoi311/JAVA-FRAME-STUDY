package com.shiyanlou.springboot.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource(value="classpath:shiyanlou.properties")  // 加载classpath目录下的shiyanlou.properties文件
public class ShiyanlouController{
    @Value("${shiyanlou.test}")
    private String shiyanlou;

    @RequestMapping("shiyanlou")
    public String shiyanlou(){
        return shiyanlou;
    }
}