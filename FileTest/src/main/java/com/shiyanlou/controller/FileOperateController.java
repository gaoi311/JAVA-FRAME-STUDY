package com.shiyanlou.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileOperateController {
    @RequestMapping(value="/upload")
    public String upload(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws Exception {
        // 上传文件自动绑定到MultipartFile中
        // 如果文件为空，跳转到失败界面
        if(file.isEmpty()){
            return "failure";
        }else{
            // 上传路径
            String path = request.getServletContext().getRealPath("/images/");
            // 上传文件名
            String filename = file.getOriginFilename();
            // 判断路径是否存在，不存在就创建一个
            File filepath = new File(path, filename);
            if(!filepath.getParentFile().exists()){
                filepath.getParentFile.mkdirs();
            }
            System.out.println("文件路径：" + path + File.separator + filename);
            file.transferTo(new File(path + File.separator + filename));
            return "success";
        }
    }
}