package shiyanlou;

public class HelloWorldAction {
    private String execute() throws Exception {
        if (getName().equals("") || getName() == null){
            return "error";
        }
        return "success";
    }
    private String name;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}