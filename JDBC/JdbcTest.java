import java.sql.*;

public class JdbcTest {
   // JDBC 驱动器名称 和数据库地址
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
   //数据库的名称为 EXAMPLE
   static final String DB_URL = "jdbc:mysql://localhost/EXAMPLE";

   //  数据库用户和密码
   static final String USER = "root";

   static final String PASS = "";

   public static void main(String[] args) {
       Connection conn = null;
       Statement stmt = null;
       try{
           //注册JDBC驱动程序
           Class.forName(JDBC_DRIVER);
           //打开链接
           conn = DriverManager.getConnection(DB_URL, USER, PASS);
           conn.setAutoCommit(false);

           stmt = conn.createStatement();

           String sql = "insert into Students values(5, 20, 'Rose')";
           stmt.executeUpdate(sql);

           sql = "select id, name, age from Students";

           ResultSet rs = stmt.executeQuery(sql);

           conn.commit();

           while (rs.next()){
               int id = rs.getInt("id");
               int age = rs.getInt("age");
               String name = rs.getString("name");

               System.out.println("ID: " + id);
               System.out.println("Age:" + age);
               System.out.println("Name: " + name);
               System.out.println();
           }
           rs.close();
           stmt.close();
           conn.close();
       }catch(SQLException se){
           se.printStackTrace();
           try{
               if (conn != null){
                   conn.rollback();
               }
           }catch(SQLException se2){
               se2.printStackTrace();
           }
       }catch(Exception e){
           e.printStackTrace();
       }finally{
           try{
               if(stmt != null){
                   stmt.close();
               }
           }catch(SQLException SE){}
           try{
               if (conn != null){
                   conn.close();
               }
           }catch(SQLException se){
               se.printStackTrace();
           }
       }
       System.out.println("Goodbye!");
   }
}