package shiyanlou.hibernate.ejb3;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import shiyanlou.hibernate.ejb3.User;

public class UserTest {

    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                //如果不写配置文件名就会使用默认配置文件名 hibernate.cfg.xml
                .configure("hibernate.cfg.xml")
                .build();
        // 建立 sessionFactory
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        User user = new User();
        user.setId(1);
        user.setUsername("admin");
        user.setPassword("admin");

        session.save(user);

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }

}